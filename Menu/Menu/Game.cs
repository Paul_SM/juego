﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Menu
{
    public class Game
    {
        public int scale = 10;
        public int lenghtMapX = 38;
        public int lenghtMapY = 20;
        public int[,] cuadros;
        private List<Square> Menu;
        
        public enum Dirrecion { Izquierda, Abajo, Derecha, Arriba }
        public Dirrecion ActualDirrecion = Dirrecion.Derecha;

        private Square food = null;
        private Random oRandom = new Random();
        private int puntos = 0;

        private Square bombs = null;

        PictureBox oPicurebox;
        Label labelPoint;

        private int PosInX
        {
            get
            {
                return lenghtMapX / 2;
            }
        }
        private int PosInY
        {
            get
            {
                return lenghtMapY / 2;
            }
        }

        public bool Perder
        {
            get
            {
                foreach (var oSquare in Menu)
                {
                    if (Menu.Where(d => d.Y == oSquare.Y && d.X == oSquare.X && oSquare != d).Count() > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public Game(PictureBox oPictureBox, Label labelPoint)
        {
            this.labelPoint = labelPoint;
            this.oPicurebox = oPictureBox;
            Reset();
        }
        public void Reset()
        {
            Menu = new List<Square>();
            Square oInSquare = new Square(PosInX, PosInY);
            Menu.Add(oInSquare);
            cuadros = new int[lenghtMapX, lenghtMapY];
            for (int j = 0; j < lenghtMapX; j++)
            {
                for (int i = 0; i < lenghtMapY; i++)
                {
                    cuadros[j, i] = 0;
                }
            }
        }
        public void Show()
        {
            Bitmap bmp = new Bitmap(oPicurebox.Width, oPicurebox.Height);
            for (int j = 0; j < lenghtMapX; j++)
            {
                for (int i = 0; i < lenghtMapY; i++)
                {
                    if (Menu.Where(d => d.X == j && d.Y == i).Count() > 0)
                    {
                        PaintPixel(bmp, j, i, Color.Black);
                    }
                    else
                    {
                        PaintPixel(bmp, j, i, Color.Transparent);
                    }
                }
            }

            //Comida
            if (food != null)
            {
                PaintPixel(bmp, food.X, food.Y, Color.Gray);
            }

            oPicurebox.Image = bmp;

            labelPoint.Text = puntos.ToString();
        }
        private void PaintPixel(Bitmap bmp, int x, int y, Color color)
        {
            for (int j = 0; j < scale; j++)
            {
                for (int i = 0; i < scale; i++)
                {
                    bmp.SetPixel(j + (x * scale), i + (y * scale), color);
                }
            }
        }

        public void Next()
        {
            if (food == null)
            {
                Getfood();
            }

            GetHistorial();

            switch (ActualDirrecion)
            {
                case Dirrecion.Derecha:
                    if (Menu[0].X == (lenghtMapX - 1))
                    {
                        Menu[0].X = 0;
                    }
                    else
                    {
                        Menu[0].X++;
                    }
                    break;

                case Dirrecion.Izquierda:
                    if (Menu[0].X == 0)
                    {
                        Menu[0].X = lenghtMapX-1;
                    }
                    else
                    {
                        Menu[0].X--;
                    }
                    break;

                case Dirrecion.Abajo:
                    if (Menu[0].Y == (lenghtMapY - 1))
                    {
                        Menu[0].Y = 0;
                    }
                    else
                    {
                        Menu[0].Y++;
                    }
                    break;

                case Dirrecion.Arriba:
                    if (Menu[0].Y == 0)
                    {
                        Menu[0].Y = lenghtMapY-1;
                    }
                    else
                    {
                        Menu[0].Y--;
                    }
                    break;
            }
            GetSigMovimiento();

            SnakeEat();

            
        }

        private void SnakeEat()
        {
            if(Menu[0].X == food.X && Menu[0].Y == food.Y)
            {
                food = null;
                puntos ++;

                Square UltimoSq = Menu[Menu.Count - 1];
                Square oSquare = new Square(UltimoSq.X_old, UltimoSq.Y_old);
                Menu.Add(oSquare);
            }
        }
        private void GetHistorial()
        {
            foreach (var oSquare in Menu)
            {
                oSquare.X_old = oSquare.X;
                oSquare.Y_old = oSquare.Y;
            }
        }
        private void GetSigMovimiento()
        {
            if (Menu.Count > 1)
                for (int i = 1; i < Menu.Count; i++)
                {
                  Menu[i].X = Menu[i - 1].X_old;
                  Menu[i].Y = Menu[i - 1].Y_old;
                }
        }

        private void Getfood()
        {
            int X = oRandom.Next(0, lenghtMapX - 1);
            int Y = oRandom.Next(0, lenghtMapY - 1);

            food = new Square(X, Y);
        }
    }

    public class Square
    {
        public int X, Y, X_old, Y_old;
        public Square(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
            this.X_old = X;
            this.Y_old = Y;
        }
    }
}
