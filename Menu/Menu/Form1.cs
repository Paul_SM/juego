﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace Menu
{
    public partial class Form1 : Form
    {
        Game oGame;

        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            oGame = new Game(pbxStage, lblPuntos);
            timer1.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e) //Boton de Play
        {
            SoundPlayer player = new SoundPlayer();
            player.SoundLocation = (@"Music\Sonar.wav");
            player.Play();
        }

        private void btnPause_Click(object sender, EventArgs e) //Boton de Pause
        {
            SoundPlayer player = new SoundPlayer();
            player.Stop();
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("-Daniela Ruelas \n-Emmanuel Salcedo" +
                "\n-Manuel Fajardo \n-Paul Solorzano", "Creditos",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!oGame.Perder)
            {
                oGame.Next();
                oGame.Show();
            }
            else
            {
                timer1.Enabled = false;
                MessageBox.Show("Perdiste :,c");
            }

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.W)
            {
                oGame.ActualDirrecion = Game.Dirrecion.Arriba;
            }
            if (e.KeyCode == Keys.A)
            {
                oGame.ActualDirrecion = Game.Dirrecion.Izquierda;
            }
            if (e.KeyCode == Keys.S)
            {
                oGame.ActualDirrecion = Game.Dirrecion.Abajo;
            }
            if (e.KeyCode == Keys.D)
            {
                oGame.ActualDirrecion = Game.Dirrecion.Derecha;
            }
        }
    }
}
